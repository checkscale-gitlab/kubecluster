
## Deploy Kubernetes cluster
1 - Install all required Software [install_software.yml](install_software.yml)
```
$ ansible-playbook -i hosts.newtf install_software.yml
```

2 - Optional - Configure Firewalld [configure_firewalld.yml](configure_firewalld.yml)
```
$ ansible-playbook -i hosts.newtf configure_firewalld.yml
```

3 - Optional - Configure Disks for Persistent Volumes [create_local_volumes.yml](create_local_volumes.yml)
```
$ ansible-playbook -i hosts.newtf create_local_volumes.yml
```

4 - Deploy Kubernetes cluster [deploy_kubernetes_cluster.yml](deploy_kubernetes_cluster.yml)
```
$ ansible-playbook -i hosts.newtf deploy_kubernetes_cluster.yml
```
---
## Reset current cluster [reset_kubernetes_cluster.yml](reset_kubernetes_cluster.yml)
```
$ ansible-playbook -i hosts.newtf reset_kubernetes_cluster.yml
```
---

## Get current cluster configuration [get_kubeconfig.yml](get_kubeconfig.yml)
```
$ ansible-playbook -i hosts.newtf get_kubeconfig.yml -l leaders


$ KUBECONFIG=~/.kube/config.kubecluster kubectl get nodes
NAME      STATUS   ROLES    AGE   VERSION
master    Ready    master   20h   v1.18.8
worker1   Ready    <none>   20h   v1.18.8
worker2   Ready    <none>   20h   v1.18.8
worker3   Ready    <none>   20h   v1.18.8
```
