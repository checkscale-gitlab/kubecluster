# instance the provider
provider "libvirt" {
  uri = "qemu:///system"
}

# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "base-image" {
  name = "base-image"
  source = "https://cloud-images.ubuntu.com/releases/bionic/release/ubuntu-18.04-server-cloudimg-amd64.img"
  format = "qcow2"
  pool = "${var.kvm_pool}"
}

# 25GB image size
resource "libvirt_volume" "base-image-resized" {
  name           = "disk"
  base_volume_id = "${libvirt_volume.base-image.id}"
  size           = 26843545600
  pool = "${var.kvm_pool}"
}

# 25GB emty disk size
resource "libvirt_volume" "base-data-disk" {
  name           = "base-data-disk"
  size           = 26843545600
  pool = "${var.kvm_pool}"
}

# Create a network for our VMs
resource "libvirt_network" "isolated_network" {
   name = "isolated_network"
   addresses = ["${var.infra-lab-network}"]
   mode = "nat"
   dhcp {
        enabled = true
   }
}



data "template_file" "user_data" {
  template = "${file("${path.module}/configs/user_config.cfg")}"
}
