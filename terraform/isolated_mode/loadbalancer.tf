


data "template_file" "network_config-loadbalancer" {
  template = "${file("${path.module}/configs/network_config-loadbalancer.cfg")}"
}


resource "libvirt_cloudinit_disk" "commoninit-loadbalancer" {
  name = "${var.infra-node-prefix}-commoninit-loadbalancer.iso"
  user_data = "${data.template_file.user_data.rendered}"
  network_config = "${data.template_file.network_config-loadbalancer.rendered}"
  pool = "${var.kvm_pool}"
}


resource "libvirt_volume" "loadbalancer" {
  name           = "${var.infra-node-prefix}-loadbalancer.qcow2"
  base_volume_id = "${libvirt_volume.base-image-resized.id}"
  pool = "${var.kvm_pool}"
}



resource "libvirt_domain" "loadbalancer" {
  name = "${var.infra-node-prefix}-loadbalancer"
  memory = "512"
  vcpu = 1

  cloudinit = "${libvirt_cloudinit_disk.commoninit-loadbalancer.id}"


  network_interface {
    network_id = "${libvirt_network.isolated_network.id}"
    network_name = "isolated_network"
  }


  # IMPORTANT
  # Ubuntu can hang is a isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
      type        = "pty"
      target_type = "virtio"
      target_port = "1"
  }

  disk {
       volume_id = "${libvirt_volume.loadbalancer.id}"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }

}
