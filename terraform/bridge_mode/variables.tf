variable "infra-os-base" {
  type = string
  default = "https://cloud-images.ubuntu.com/releases/bionic/release/ubuntu-18.04-server-cloudimg-amd64.img"
  description = "Infrastructure OS Base Image (URL or QCOW)"
}

variable "private_key_path" {
  type = string
  default = "../keys/provision"
  description = "SSH Private Key"
}

variable "infra-lab" {
  type = string
  default = "lab"
  description = "Infrastructure Environment Name"
}


variable "kvm_pool" {
  type = string
  default = "labs"
  description = "Pool for disk images and other resources (must be previously created)."

    # $ virsh pool-define-as --name labs --type dir --target /media/work/kvm
    # Pool default defined
    # Set pool to be started when libvirt daemons starts:

    # $ virsh pool-autostart labs
    # Pool default marked as autostarted
    # Start pool:

    # $ virsh pool-start labs
    # Pool default started

}

variable "kvm_bridge_interface" {
  type = string
  default = "br0"
  description = "Local Hypervior Bridge Interface."
}
